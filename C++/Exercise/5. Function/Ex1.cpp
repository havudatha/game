#include <iostream>
using namespace std;

long long factor(int n);
int main(){
	int n;
	cin >> n;
	cout << factor(n);
	return 0;
} 

long long factor(int n){
	
	if(n == 1){
		return 1;
	}
	int s = 1;
	s = n * factor(n - 1);
	return s;
	
}
