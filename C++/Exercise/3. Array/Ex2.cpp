#include<iostream>
#include<stdlib.h>
using namespace std;

int input_Array_2 (int a[][10], int n, int m);
int sum_Array_2 (int a[][10], int n, int m);
int output_Array_2 (int a[][10], int n, int m);
int search_Max_Row_Array_2 (int a[][10], int n, int m);
int sum_Border_Area_Array_2 (int a[][10], int n, int m);
int sum_Arra_And_Arrb (int a[][10],int b[][10], int n, int m);
int sum_Odd (int a[][10], int n, int m);

int main(){
	
	int n,m;
	int a[10][10],b[10][10];
	cin >> n >> m;
	input_Array_2(a,n,m);
//	input_Array_2(b,n,m);
//	sum_Array_2(a,n,m);
//	output_Array_2(a,n,m);
//	sum_Border_Area_Array_2(a,n,m);
//	search_Max_Row_Array_2(a,n,m);
//	sum_Arra_And_Arrb(a,b,n,m);
	sum_Odd(a,n,m);
	return 0;
}

int input_Array_2 (int a[][10], int n, int m){
	cout << "Nhap mang 2 chieu: " << endl;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			cout << "a[" << i << "][" << j <<"] = ";
			cin >> a[i][j];
		}
	}
	return 0;
}

int sum_Array_2 (int a[][10], int n, int m){
	int sum = 0;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			
			sum += a[i][j];
		}
	}
	cout << sum;
	return 0;
}

int output_Array_2 (int a[][10], int n, int m){
	cout << "Xuat mang 2 chieu: " << endl;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			cout << "a[" << i << "][" << j <<"] = " << a[i][j] << " ";
		}
		cout << endl;
	}
	return 0;
}

int search_Max_Row_Array_2 (int a[][10], int n, int m){
	
	for(int i = 0; i < n; i++){
		int max = a[i][0];
		for(int j = 0; j < m; j++){
			if(max < a[i][j]){
				max = a[i][j];
			}
		}
		cout << max << " ";
	}
	return 0;
}

int sum_Border_Area_Array_2 (int a[][10], int n, int m){
	
	int sum = 0;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			if( i == 0 || j == 0 || i == n-1 || j == m-1){
				sum += a[i][j];
			}
		}
	
	}
	
	cout << sum;
	return 0;
}

int sum_Arra_And_Arrb (int a[][10],int b[][10], int n, int m){
	int c[10][10];
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			c[i][j] = a[i][j] + b[i][j];
		}
	}
	cout << "Tong 2 ma tran: " << endl;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			cout << "a[" << i << "][" << j <<"] = " << c[i][j] << " ";
		}
		cout << endl;
	}
	
}

int sum_Odd (int a[][10], int n, int m){
	int sum = 0;
	for(int i = 0; i < n; i++){
		for(int j = 0; j < m; j++){
			if(a[i][j] % 2 == 1){
				sum += a[i][j];
			}
		}
	}
	cout << sum;
}


