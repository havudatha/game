#include<iostream>
using namespace std;


void input_Array (int *a, int n);
void output_Array (int *a, int n);
void sort_array (int* &a, int n);

int main(){
	 
	 int n;
	 cin >> n;
	 int *a = new int[n];
	 input_Array(a,n);
	 sort_array(a,n);
	 output_Array(a,n);
	 return 0;
}

void input_Array (int *a, int n){
	cout << "Nhap mang: " << endl;
	for(int i = 0; i < n;i++){
		cout << "a[" << i << "]= ";
		cin >> a[i];	
	}
}


void output_Array (int *a, int n){
	cout << "Xuat mang: " << endl;
	for(int i = 0; i < n;i++){
		cout << "a[" << i << "]= " << a[i] << endl;
			
	}
}

void sort_array (int* &a, int n){
	for(int i = n-1; i > 0; i--){
		for(int j = 0; j < i; j++){
			if(a[j] > a[j+1]){
				int tmp = a[j+1];
				a[j+1] = a[j];
				a[j] = tmp;
			}
		}
	}

}
