#include<iostream>
using namespace std;

struct Student {
	string name;
	int age;
	
	friend istream& operator >>(istream &is, Student &student){
//		cout << "Name: ";
//		is >> student.name;
		cout << "Age: ";
		is >> student.age;
		return is;
	}
	
	friend ostream& operator <<(ostream &os, Student student){
		os << student.name << endl;
		os << student.age;
		return os;
	}
	
	friend Student operator+(Student stu1, Student stu2){
		Student stu3;
		stu3.age = stu1.age + stu2.age;
		return stu3;
	}
	
};


int main(){
	Student stu1,stu2;
	cin >> stu1 >> stu2;
	Student stu3;
	stu3 = stu1 + stu2;
	cout << stu3;
	return 0;
}
