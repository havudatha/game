#include<iostream>
#include<string>
using namespace std;

int main(){
	// substr
	string str = "havudat";
	string str1;
	str1 = str.substr(4);
	cout << str1 << endl;
	//find
	int str2;
	string str3 = "vu";
	str2 = str.find(str3);
	cout << str2 << endl;
	//erase
	string str4 = str.erase(2,2);
	cout << str4 <<endl;
	//replace
	string str5 =str.replace(0,2,"**");
	cout << str5 <<endl;
	//compare
	int str6;
	str6 = str1.compare(str4);
	cout << str6 <<endl;
	//insert
	string str7;
	str7 = str4.insert(2,"vu");
	cout << str7 << endl;
	//str.length(), str.size()
	cout << str4.size() <<endl;
	cout << str1.length();
	return 0;
}
