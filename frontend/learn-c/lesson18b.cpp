#include<iostream>
#include<math.h>
using namespace std;

#define PI 3.14

struct Circle {
	int r;
	
	friend istream& operator>>(istream &is, Circle &circle){
		cout << "Input circle: ";
		is >> circle.r;
		return is;
	}
	
	friend ostream& operator<<(ostream &os, Circle circle){
		cout << circle.r;
		return os;
	}
	
	float chu_vi(){
		float cv;
		cv = (float) r*2*PI;
		return cv;
	}
	
	float dien_tich(){
		float dt;
		dt = (float) pow(r,2)*PI;
		return dt;
	}
};



int main(){
	
	Circle cir;
	cin >> cir;
	cout << cir.chu_vi() <<endl;
	cout << cir.dien_tich();
	return 0;
}
