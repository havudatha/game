#include<iostream>
#include<string>
using namespace std;

string delete_space_start (string &str);
string delete_space_end (string &str);
string delete_space_between(string &str);
string change_tolower (string &str);
string change_word_toupper(string &str);

int main(){
	
	string str1;
	cout << "Input String: ";
	getline(cin,str1);
	delete_space_start(str1);
	delete_space_end(str1);
	delete_space_between(str1);
	change_tolower(str1);
	change_word_toupper(str1);
	cout << str1;
	return 0;
}


string delete_space_start (string &str){
	for(int i = 0; i < (str.length() - 1);i++){
		if(str[i] == ' '){
			str.erase(i,1);
			i -=1;	
		}else {
			break;
		}
	}
		return str;
}

string delete_space_end (string &str){
	for(int i = (str.length() - 1); i > 0; i--){
		if(str[(str.length() - 1)] == ' '){
			str.erase((str.length() - 1),1);
		}
	}
	return str;
}

string delete_space_between (string &str){
	int i = 0;
	while(i < (str.length() - 1)){
		if(str[i] == ' ' && str[i+1] == ' '){
			str.erase(i,1);
		}else {
			i++;
		}	
	}
	return str;
}

string change_tolower (string &str){
		for(int i = 0; i < (str.length()); i++){
		if(str[i] > 64 && str[i] < 91){
			str[i] = tolower(str[i]);
		}
	}
	return str;
}

string change_word_toupper (string &str){
	str[0] = toupper(str[0]);
	for(int i = 0; i < (str.length() - 1); i++){
	if(str[i] == ' '){
			str[i+1] = toupper(str[i+1]);
		}
	}
	return str;
}
