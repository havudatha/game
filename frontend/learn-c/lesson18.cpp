#include<iostream>
#include<string>
using namespace std;

struct Date {
	int day;
	int month;
	int year;
	
	Date(){
	}
	
	Date(int _day, int _month, int _year){
		day = _day;
		month = _month;
		year = _year;
	}
};

struct People {
	string name;
	int age;
	long number;
	Date dob;
	
	People(){
		
	}
	
	People(string _name, int _age, long _number, Date _dob){
		name = _name;
		age = _age;
		number = _number;
		dob = _dob;
	}
	
	void print() {
		cout << "Name: " <<name << ", "<<" Tuoi: "  << age << ", " << "Number: " << number;
	}
	
	void input(){
	
		
		getline(cin,name);
		cin.ignore();
		cin >> age;
		cin >> number;
	}
};



int main(){
	People p;
	p.input();
	p.print();
	return 0;
}
