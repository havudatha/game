#include<stdio.h>
#include<iostream>
using namespace std;

void math_char (int a, int b){
	cout << a << " + " << b << " = " << a+b << endl;
	cout << a << " - " << b << " = " << a-b << endl;
	cout << a << " / " << b << " = " << a/b << endl;
	cout << a << " * " << b << " = " << a*b;
}

int main() {
	int a,b;
	cout << "Input number a: ";
	cin >> a;
	cout << "Input number b: ";
	cin >> b;
	math_char(a,b);
	
	return 0;
}

